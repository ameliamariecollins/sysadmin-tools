#!/bin/sh
# Author: Amelia Marie Collins <amelia@appliedneural.net>

#.    usage: PROGRAM [options] spec [spec ...]
#.
#.        -v     Display reason host was unavailable
#.        -a     Show all available IPs instead of the first one only
#.
#.        -h     Print usage information
#.
#.    The "spec" is an IPv4 network range specified in CIDR notation, a
#.    hyphenated range, or a single address.
#.
#.    Example:
#.        PROGRAM 192.168.0.0/24
#.        PROGRAM 192.168.0.1-192.168.0.20
#.        PROGRAM 192.168.0.1

# Provide usage information
usage () {
    # Display an optional message, if passed
    MSG="$@"
    [ "$MSG" ] && echo $MSG

    # - Extract usage from special comments
    # - Remove comment sigil and padding
    # - Interpolate script name
    egrep '^#\.' $0                             \
	| sed 's/^#\.\ \{0,4\}//'               \
	| sed "s/PROGRAM/$(basename $0)/"
    exit
}

# Parse command-line options
get_options() {
    unset SHOW_ALL VERBOSE SPECS

    local OPTIND opt
    while getopts vah OPT
    do
	case $OPT in
	    v) VERBOSE="true";;
	    a) SHOW_ALL="true";;

	    h) usage; exit;;
	esac
    done
    shift $(expr $OPTIND - 1)

    # Ensure specs were provided
    SPECS="$@"
    if ! [ "$SPECS" ]
    then
	usage
	exit 2
    fi
}

octet() {
    IP="$1"
    OCTET="$2"

    # Extract a single octet from the IP address
    echo $IP | cut -d. -f$OCTET
}

scan_ip() {
    IP="$1"

    # IP is not available if it resolves
    if ! host $IP | grep -q 'no PTR record\|(NXDOMAIN)'
    then
	HOST=$(host $IP | awk '{print $5}' | sed 's/\.$//')
	[ "$VERBOSE" ] && echo "($IP is $HOST)"
	return
    fi

    # IP is not available if it is pingable
    if pingstatus $IP
    then
	[ "$VERBOSE" ] && echo "($IP was pingable)"
	return
    fi

    # IP is presumed available
    FOUND='yes'
    echo $IP

    # Just need to show the first available address? Done.
    [ "$SHOW_ALL" ] || exit
}

scan_range() {
    SPEC="$@"

    # If CIDR, resolve to hyphenated range
    if echo $SPEC | grep -q /
    then
	SPEC=$(netmask -r $SPEC | awk '{print $1}')
    fi

    # If hyphenated, resolve to space-separated address pair
    if echo $SPEC | grep -q -- -
    then
	SPEC=$(echo $SPEC | sed 's:-: :')
    fi

    # Split address pair
    START=$(echo $SPEC | cut -d' ' -f1)
    END=$(echo $SPEC | cut -d' ' -f2)

    # Single IP only? Just scan that address only
    if ! [ "$END" ]
    then
	scan_ip $START
	return
    fi

    # Separate each octet of the IP address
    START_A=$(octet $START 1)
    START_B=$(octet $START 2)
    START_C=$(octet $START 3)
    START_D=$(octet $START 4)

    END_A=$(octet $END 1)
    END_B=$(octet $END 2)
    END_C=$(octet $END 3)
    END_D=$(octet $END 4)

    # Scan through the range
    for A in $(seq $START_A $END_A)
    do
	for B in $(seq $START_B $END_B)
	do
	    for C in $(seq $START_C $END_C)
	    do
		for D in $(seq $START_D $END_D)
		do
		    scan_ip "$A.$B.$C.$D"
		done
	    done
	done
    done
}

main() {
    unset FOUND
    get_options $@

    # Scan all specs on command line
    for SPEC in $SPECS
    do
	scan_range $SPEC
    done

    # Successful exit status if an address was available, otherwise fail
    if [ "$FOUND" ]
    then
	exit 0
    else
	exit 1
    fi
}

main $@
